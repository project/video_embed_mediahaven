# Changelog
All Notable changes to `video_embed_mediahaven`.

## [Unreleased]

## [7.x-1.3]

### Removed
* MediaHaven credentials is not a video style setting.

## [7.x-1.2]

### Fixed
* Fix incorrect usage of substr().

## [7.x-1.1]

### Fixed
* Fix incorrect usage of cache_clear_all.

## [7.x-1.0]

### Added
* Handler for videos hosted on a MediaHaven instance.


[Unreleased]: https://git.drupalcode.org/project/video_embed_mediahaven/compare/7.x-1.3...7.x-1.x
[7.x-1.3]: https://git.drupalcode.org/project/video_embed_mediahaven/compare/7.x-1.2...7.x-1.3
[7.x-1.2]: https://git.drupalcode.org/project/video_embed_mediahaven/compare/7.x-1.1...7.x-1.2
[7.x-1.1]: https://git.drupalcode.org/project/video_embed_mediahaven/compare/7.x-1.0...7.x-1.1
[7.x-1.0]: https://git.drupalcode.org/project/video_embed_mediahaven/tags/7.x-1.0
